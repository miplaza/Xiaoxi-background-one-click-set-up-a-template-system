# 小熙后台一键搭建模板系统
![](https://img.shields.io/badge/Vue-2.5.2-green.svg)
![](https://img.shields.io/badge/axios-0.21.0-blue.svg)
## 简介
只需提供您的数据，最基本的表头信息就能一键生成增、删、查、改的前端代码，具体业务流程您只需要接入您的api即可。
目前还在开发中，预想提供以下功能。
| 封装组件 | 是否完成 |
|--|--|
|页面布局文字颜色自定义  | √ |
|增删查改高自定义表格组件  | 进行中 |
|echarts图表组件  |  |
|高自定义卡片组件  |  |
|系统日志组件  |  |

## 1、页面布局文字颜色自定义

通过对config文件夹下的global.js 设置，可以自定义网站标题，欢迎语，头部背景颜色，头部文本颜色，侧边栏背景颜色，侧边栏文本颜色，侧边栏选中颜色，侧边栏导航栏定义。需要注意的是侧边栏的导航新增页面，需要自行新建页面，并在router文件夹下的index.js注册路由，再往config的global.js填写路由url才能正确被导航到。后期将会考虑自动生成。

```
let webSiteSetting={
    title:"小熙后台一键搭建模板系统[可自定义]",
    welecome_msg:"欢迎您来到小熙后台一键搭建模板系统，请修改global.js文件完善您的后台框架，本系统提供最简单的后台搭建模板，您可以在几分钟实现简单后台搭建。",
    header_bg_color:"",
    header_text_color:"",
    slide_bg_color:"",
    slide_text_color:"",
    slide_select_color:"",
    slide_selects:[
        {
            icon:"el-icon-menu",
            name:"主页",
            type:"el-menu-item",
            index:"/"
        },
        {
            icon:"el-icon-user-solid",
            name:"组件",
            type:"el-submenu",
            children:[
                {
                    name:"发布任务",
                    index:"publish"
                },{
                    name:"表格渲染",
                    index:"managertable"
                }
            ]
        },
    ]
}
```
![页面布局文字颜色自定义](https://img-blog.csdnimg.cn/20210510151900338.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)

## 2、增删查改高自定义表格组件
此模块在后台运用将会十分频繁，设计种类繁多。在此模块，我尽可能的实现多种表单组件的合理运用，从而节省您的开发时间和学习成本。同样如果需要特色需求，在这里你也可以合理运行插槽去实现您具体的表格样式与功能。
在views下的ManagerTable.vue有一个丰富的demo例子。
### 2.1 表格数据定义
表格数据定义有规定的格式，包含表头定义hedaer以及数据定义data以及页码。
- hedaer表头定义，是定义表头宽度，显示内容名称，列名，单元格属性。
- data数据定义，无需关心，只需要和表头定义的显示内容名称一致即可。
- 页码，填写总的页码，生成页码器。

header案例
```
"header": [
    {
        "label": "日期",
        "width": "",
        "prop": "date"
    },
    {
        "label": "姓名",
        "width": "",
        "prop": "name"
    }
  ],
```
以上的代码就是代表两列，第一列的数据提供是date，第二列的数据提供是name,宽度全部是自动。

data案例
```
"data": [
    {
        "date": "2016-05-08",
        "name": "第二页"
    },
    {
        "date": "2016-05-06",
        "name": "第二页"
    }
]
```
以上的代码就是我们一般后端传来的数组，里面有对应的属性，只需要header与data相对应的列则会渲染出来。
### 2.2 hedaer全部可调参数(暂时)

| 属性 | 参数 |
|--|--|
|label  | 列名|
|width  | 宽度 |
|prop  | 显示内容的属性名 |
|type  | 单元格格式(默认文本显示、html代码模式(html)、tag模式(tag|?)、switch模式(switch)、插槽模式(slot)) |
### 2.3 最基础的表格
以下是html代码
```
<template>
    <div>
        <Table 
            :tableData="table_data" 
            @current-change="change" 
            @add="add" 
            @edit="edit"
        >
        </Table>
    </div>
</template>
```
以下是js代码
```
<script>
import Table from '../components/Table.vue'
import axios from 'axios'
    export default {
        name: "ManagerTable",
          components: {
            Table
        },
        data() {
            return {
                "table_data": {
                    "pages": 2,
                    "header": [
                        {
                            "label": "日期",
                            "width": "",
                            "prop": "date"
                        },
                        {
                            "label": "姓名",
                            "width": "",
                            "prop": "name"
                        },
                        {
                            "label": "省份",
                            "width": "",
                            "prop": "province"
                        },
                        {
                            "label": "城市",
                            "width": "",
                            "prop": "city"
                        },
                        {
                            "label": "地址",
                            "width": "",
                            "prop": "address"
                        },
                        {
                            "label": "区号",
                            "width": "",
                            "prop": "zip"
                        }
                    ],
                    "data": [
                        {
                            "date": "2016-05-08",
                            "name": "第二页",
                            "province": "上海",
                            "city": "普陀区",
                            "address": "上海市普陀区金沙江路 1518 弄",
                            "zip": 200333
                        },
                        {
                            "date": "2016-05-06",
                            "name": "第二页",
                            "province": "上海",
                            "city": "普陀区",
                            "address": "上海市普陀区金沙江路 1518 弄",
                            "zip": 200333
                        }
                    ]
                }
            };
        },
        mounted(){
            
        },
        computed:{
            
        },
        methods:{

            /**
             * 页码切换
             * 参数：页码
            */
            change(i){
                console.log(i)
            },

            /**
             * @typedef {Object} cb
             * @property {Boolean} 是否修改成功
             * @property {String} 提示消息
             */
            /**
             * 添加一行
             * 参数：新行数据,回调函数(boolean,string) 第一个判断是否修改成功，第二个为消息提示
             * 当回调函数为true时，会自动在头部添加新行，为false则不会
             * @param {Object} 新行数据
             * @param {cb} callback 第一个判断是否修改成功，第二个为消息提示
            */
            add(data,callback){
                console.log(data)
                callback(true,"新增成功了！")
                //callback(false,"添加失败！")
            },

            /**
             * 编辑一行
             * 参数：旧行数据，当前行新数据，所属行下标,回调函数(boolean,string) 第一个判断是否修改成功，第二个为消息提示
             * 当回调函数为true时，会自动更新当前行数据和关闭对话框，为false则不会
             * @param {Object} 当前行旧数据
             * @param {Object} 当前行新数据
             * @param {cb} callback 第一个判断是否修改成功，第二个为消息提示
            */
            edit(old_data,data,i,callback){
                console.log(old_data,data,i)
                callback(true,"修改成功了！")
            }
        }
    };
</script>
```
上面的代码演示了一个最基础的用法，就可以生成了增加、删除、查找、修改的前端代码。
![最基础的表格](https://img-blog.csdnimg.cn/20210510153309665.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)
新增
![新增](https://img-blog.csdnimg.cn/20210510153401836.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)
编辑
![编辑](https://img-blog.csdnimg.cn/20210510153429510.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)
删除
![删除](https://img-blog.csdnimg.cn/20210510153501268.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)
查找
![查找](https://img-blog.csdnimg.cn/2021051015354938.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzM1Mzg5Ng==,size_16,color_FFFFFF,t_70)
## 部署环境
### 程序安装
```
npm install
```

### 启动服务
```
npm run serve
```

### 打包发布
```
npm run build
```