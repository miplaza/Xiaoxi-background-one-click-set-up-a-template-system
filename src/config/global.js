/**
 * 小熙后台一键搭建模板系统
 * 以下是部分自定义内容，修改即可完成大部分主要框架设计
 */
let webSiteSetting={
    title:"小熙后台一键搭建模板系统[可自定义]",
    welecome_msg:"欢迎您来到小熙后台一键搭建模板系统，请修改global.js文件完善您的后台框架，本系统提供最简单的后台搭建模板，您可以在几分钟实现简单后台搭建。",
    header_bg_color:"",
    header_text_color:"",
    slide_bg_color:"",
    slide_text_color:"",
    slide_select_color:"",
    slide_selects:[
        {
            icon:"el-icon-menu",
            name:"主页",
            type:"el-menu-item",
            index:"/"
        },
        {
            icon:"el-icon-user-solid",
            name:"组件",
            type:"el-submenu",
            children:[
                {
                    name:"发布任务",
                    index:"publish"
                },{
                    name:"表格渲染",
                    index:"managertable"
                }
            ]
        },
    ]
}


export default{
    webSiteSetting,//网站设置
}