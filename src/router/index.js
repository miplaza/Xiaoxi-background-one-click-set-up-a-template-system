import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Welecome from '../views/Welecome.vue'
import Publish from '../views/Publish.vue'
import ManagerTable from '../views/ManagerTable.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children:[
      {
        path: '/',
        name: 'Welecome',
        component: Welecome
      },
      {
        path: 'publish',
        name: 'Publish',
        component: Publish
      },
      {
        path: 'managertable',
        name: 'ManagerTable',
        component: ManagerTable
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
