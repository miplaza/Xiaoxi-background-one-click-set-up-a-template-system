import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'

import ElementUI from'element-ui';
import'element-ui/lib/theme-chalk/index.css';
import router from './router'
import global from "@/config/global.js"    //注意文件路径，实际路径以项目目录结构为准
Vue.use(ElementUI);
Vue.config.productionTip = false
//定义全局用户信息
Vue.prototype.$userMsg = {name:"黄鉴熙"};
//定义全局请求服务器地址
Vue.prototype.$baseUrl = "";

Vue.prototype.$global = global;
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
